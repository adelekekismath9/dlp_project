import tokenize

def tokenize_file(file_path):
    with open(file_path, 'rb') as file:
        return list(tokenize.tokenize(file.readline))
        

