import ast
from collections import defaultdict
import colorama

MAX_FILE_LINES = 200
MAX_LINE_LENGTH = 79

def check_imports(tree):
    imports = set()
    for node in ast.iter_child_nodes(tree):
        if isinstance(node, ast.Import):
            for alias in node.names:
                imports.add(alias.name)
        elif isinstance(node, ast.ImportFrom):
            if node.module:
                imports.add(node.module)
    # Verification des utilisations des imports
    used_imports = set()
    for node in ast.walk(tree):
        if isinstance(node, ast.Name) and isinstance(node.ctx, ast.Load):
            used_imports.add(node.id)
    # Verification des imports" non utilises
    unused_imports = imports - used_imports
    for unused_import in unused_imports:
        print(colorama.Fore.GREEN+ f"{unused_import} est importé, mais jamais utilisé"+ colorama.Style.RESET_ALL)
    if(len(unused_imports) > 3):
        return 0
    elif(len(unused_imports) == 0):
        return 1
    else:
        return 1- round(len(unused_imports)/10,1)

def check_code_comments(tokens):
    nbr_comment = 0
    for token in tokens:
        if token.type == 61:
            nbr_comment += 1
    tuple = tokens[-1].start
    nbr_ligne_code = tuple[0]
    pourcentage = nbr_comment / nbr_ligne_code
    if(pourcentage < 10):
        print(colorama.Fore.GREEN+f"Le fichier n'est pas assez commenté"+ colorama.Style.RESET_ALL)
        return 0.5
    if(10 < pourcentage < 30):
        return 1
    else:
        print(colorama.Fore.GREEN+f"Le fichier n'est pas assez commenté"+ colorama.Style.RESET_ALL)
        return round(1-pourcentage)
    
def check_file_length(tokens):
    totalLines = tokens[-1].end[0]
    if totalLines > MAX_FILE_LINES:
        print(colorama.Fore.GREEN+f"Fichier trop long : {totalLines} lignes"+ colorama.Style.RESET_ALL)
    
    if(totalLines > 400):
        return 0
    elif(totalLines <= MAX_FILE_LINES):
        return 1
    elif(totalLines < 400):
        return 1-  round( - 200/totalLines,1)

def check_line_length(tokens):
    Lines = getAllLines(tokens)
    nbLinesTooLong = 0
    for line in Lines:
        if(len(line[0])> MAX_LINE_LENGTH):
           nbLinesTooLong += 1
           print(colorama.Fore.GREEN+f"La ligne {line[1]} est trop longue "+ colorama.Style.RESET_ALL) 
    if(nbLinesTooLong /len(Lines) > 0.3 ):
        return 0 
    else :
         return 1- round(nbLinesTooLong /len(Lines),1)

def check_hardcoded_values(tree):
    constant_names = []
 
    for node in ast.walk(tree):
        if isinstance(node, ast.Assign) and isinstance(node.value, ast.Constant):
            for target in node.targets:
                if isinstance(target, ast.Name):
                    constant_names.append(target.id)
                    print(colorama.Fore.GREEN+f"Affectation d'une valeur en dur détecté pour la variable " +target.id+ colorama.Style.RESET_ALL) 
    if(len(constant_names) > 3):
            return 0
    elif(len(constant_names) == 0):
        return 1
    else:
        return  1 - round(len(constant_names)/10,1)

def check_unused_lines(tokens):
    tooMuchUnusedLines = False
    errors = 0
    for i in range(len(tokens)-2):
        if(tokens[i].line == "\n" and (tokens[i+1].line == "\n" and tokens[i+1].end[0]!=tokens[i].end[0]) and (tokens[i+2].line == "\n" and tokens[i+2].end[0]!=tokens[i+1].end[0]) ):
            errors += 1
            tooMuchUnusedLines = True
    if(tooMuchUnusedLines):
        print(colorama.Fore.GREEN+"Il y a trop de saut de ligne"+ colorama.Style.RESET_ALL) 
    Lines = getAllLines(tokens)
    if(errors/len(Lines) > 0.3 ):
        return 0 
    else :
         return 1 - round(errors/len(Lines) ,1)  

def getAllLines(tokens):
    Lines = []
    for token in tokens:
        if(token.line != "" and (token.line, token.end[0] )not in Lines):
            Lines.append((token.line, token.end[0]))
    return Lines

def check_function_refactoring(tokens,tree):
   # Dictionnaire pour stocker les blocs de lignes similaires
    duplicate_blocks = defaultdict(list)

    # Parcourir tous les noeuds de l'AST
    for node in ast.walk(tree):
        if isinstance(node, ast.FunctionDef):
            # Récupérer les lignes du corps de la fonction
            lines = [getAllLines(tokens)[line.lineno - 1] for line in node.body]

            # Ajouter les lignes au dictionnaire des blocs de lignes similaires
            duplicate_blocks[tuple(lines)].append(node.name)

    # Filtrer les blocs de lignes similaires avec au moins 3 lignes
    duplicate_blocks = {lines: functions for lines, functions in duplicate_blocks.items() if len(lines) >= 3}

    if(len(duplicate_blocks) > 1):
        print(colorama.Fore.GREEN+f"Le fichier peut etre mieux factorisé"+ colorama.Style.RESET_ALL)
        return 0
    else:
        return 1