
import ast

def parser(file_path):
    with open(file_path, "r") as file:
        source_code = file.read()
        
    return ast.parse(source_code)