import helperScoring
import tokenizer
import parserizer

#  Définir les critères de qualité et les points correspondants
criteria = {
    "importation": 2,
    "comments": 5,
    "lineLengh": 3,
    "functionRefactoring": 4,
    "fileLengh": 1,
    "constantUses": 3,
    "cleanUnneccessaryLines": 2
}
def getCodeScoring(filePath):
    tokens = tokenizer.tokenize_file(filePath)
    tree = parserizer.parser(filePath)
    result= {
            'importation': criteria["importation"] * helperScoring.check_imports(tree),
            'comments': criteria["comments"] * helperScoring.check_code_comments(tokens),
            'lineLengh': criteria["lineLengh"] * helperScoring.check_line_length(tokens),
            'functionRefactoring': criteria["functionRefactoring"] * helperScoring.check_function_refactoring(tokens,tree),
            'fileLengh': criteria["fileLengh"] * helperScoring.check_file_length(tokens),
            'constantUses' : round(criteria["constantUses"] * helperScoring.check_hardcoded_values(tree),1),
            'cleanUnneccessaryLines': criteria["cleanUnneccessaryLines"] * helperScoring.check_unused_lines(tokens)
        }

    return {'score': str(result["importation"] +
                        result["comments"] +
                        result["lineLengh"]+
                        result["functionRefactoring"]+
                        result["fileLengh"]+
                        result["constantUses"]+
                        result["cleanUnneccessaryLines"]) +"/20",
            'details': result
            }

