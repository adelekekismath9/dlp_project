import random
import math
import sys

a = 1000
b = "strinf"
sys.setrecursionlimit(10000000)

def cercle_min(points):
    random.shuffle(points)
    return _cercle_min_welzl(points, [])

def _cercle_min_welzl(points, boundary):
    if len(points) == 0 or len(boundary) == 3:
        return _cercle_min_bordure(boundary)
    else:
        p = points.pop()
        cercle = _cercle_min_welzl(points, boundary)
        if _distance(p, cercle["centre"]) <= cercle["rayon"]:
            return cercle
        else:
            boundary.append(p)
            return _cercle_min_welzl(points, boundary)

def _cercle_min_bordure(boundary):
    if len(boundary) == 0:
        return {"centre": (0,0), "rayon": 0}
    elif len(boundary) == 1:
        return {"centre": boundary[0], "rayon": 0}
    elif len(boundary) == 2:
        x1, y1 = boundary[0]
        x2, y2 = boundary[1]
        centre = ((x1 + x2) / 2, (y1 + y2) / 2)
        rayon = _distance(centre, boundary[0])
        return {"centre": centre, "rayon": rayon}
    else:
        centre, rayon = _cercle_min_ritter(boundary)
        return _cercle_min_ritter(boundary), _cercle_min_welzl(boundary, [p for p in boundary if _distance(p, centre) == rayon])

def _cercle_min_ritter(points):
    p = points[0]
    q = max(points, key=lambda x: _distance(x, p))
    r = max(points, key=lambda x: _distance(x, q))
    centre = ((q[0] + p[0] + r[0]) / 3, (q[1] + p[1] + r[1]) / 3)
    rayon = _distance(centre, r)
    return centre, rayon

def _distance(p, q):
    return math.sqrt((p[0] - q[0]) ** 2 + (p[1] - q[1]) ** 2)


points = [(0,2),(4,6),(1,1),(9,0),(10,6),(3,3),(1,8)]
cercle_min(points)